package br.com.microprojeto.orquestrador.business;

import br.com.microprojeto.orquestrador.entity.Pessoa;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RuleEngine {

    public Map parametrosCpf(String cpfPessoa) {
        Map<HttpStatus, String> erros = new HashMap<>();
        if(cpfPessoa == null){
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetro não informado.");
        }
        if("".equals(cpfPessoa)){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if (cpfPessoa != null && cpfPessoa.length() != 11) {
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        try{
            Long.parseLong(cpfPessoa);
        }catch (Exception e){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        return erros;
    }
    public Map cpfNaoLocalizado(ResponseEntity pessoa){
        Map<HttpStatus, String> erros = new HashMap<>();
        if(pessoa == null){
            erros.put(HttpStatus.NO_CONTENT, "Pessoa não localizada.");}
        return erros;
    }
    public Map parametroCadastro(Pessoa pessoa) {
        Map<HttpStatus, String> erros = new HashMap<>();
        if(pessoa.getNome() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getCpf() == null){
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getRg() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getLogradouro() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getNumLogradouro() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getCep() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getTelefone() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if(pessoa.getDdd() == null) {
            erros.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        }
        if("".equals(pessoa.getNome())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if("".equals(pessoa.getCpf())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if(pessoa.getCpf() != null && pessoa.getCpf().length() != 11){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        try{
            Long.parseLong(pessoa.getCpf());
        }catch (Exception e) {
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        if("".equals(pessoa.getRg())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if(pessoa.getRg() != null && pessoa.getRg().length() > 12){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        if("".equals(pessoa.getLogradouro())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if("".equals(pessoa.getNumLogradouro())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if("".equals(pessoa.getCep())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if(pessoa.getCep() != null && pessoa.getCep().length() != 8){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        try{
            Integer.parseInt(pessoa.getCep());
        }catch (Exception e){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        }
        if("".equals(pessoa.getDdd())){
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if(pessoa.getDdd() != null && pessoa.getDdd().length() != 2){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        }
        try{
            Integer.parseInt(pessoa.getDdd());
        }catch (Exception e){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        }
        if("".equals(pessoa.getTelefone())) {
            erros.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        }
        if(pessoa.getTelefone() != null && pessoa.getTelefone().length() != 8 && pessoa.getTelefone().length() != 9){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        }
        try{
            Integer.parseInt(pessoa.getTelefone());
        }catch (Exception e){
            erros.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        }
        return erros;
    }
}
