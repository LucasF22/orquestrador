package br.com.microprojeto.orquestrador.consumer;

import br.com.microprojeto.orquestrador.constante.FilaConstante;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RetornoConsumer {
    @RabbitListener(queues = FilaConstante.FILA_RETORNO)
    private void consumidorRetornoCadastro(String retorno){
        log.info(retorno);
    }
}
