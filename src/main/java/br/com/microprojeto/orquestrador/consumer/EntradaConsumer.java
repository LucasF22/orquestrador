package br.com.microprojeto.orquestrador.consumer;

import br.com.microprojeto.orquestrador.business.RuleEngine;
import br.com.microprojeto.orquestrador.constante.FilaConstante;
import br.com.microprojeto.orquestrador.entity.Pessoa;
import br.com.microprojeto.orquestrador.service.PessoaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class EntradaConsumer {
    @Autowired
    private PessoaService pessoaService;
    @Autowired
    private RuleEngine parameterPessoa;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = FilaConstante.FILA_ENTRADA)
    private void consumidorEntradaCadastro(String request) throws JsonProcessingException {
        //Pessoa pessoa = new ObjectMapper().readValue(request, Pessoa.class);
        Pessoa pessoa = (new Gson()).fromJson(request,Pessoa.class);
        Map<HttpStatus, String> listaErrosValidacao = parameterPessoa.parametroCadastro(pessoa);
        ResponseEntity cadastro;
        if (listaErrosValidacao.isEmpty()) {
            pessoaService.cadastrarPessoa(true, pessoa);
        }else {
            cadastro = new ResponseEntity(listaErrosValidacao.values(), HttpStatus.NOT_FOUND);
            rabbitTemplate.convertAndSend(FilaConstante.FILA_RETORNO, cadastro);
        }
    }

}
