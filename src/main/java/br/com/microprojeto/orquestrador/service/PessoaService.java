package br.com.microprojeto.orquestrador.service;

import java.util.ArrayList;
import java.util.List;

import br.com.microprojeto.orquestrador.constante.FilaConstante;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.microprojeto.orquestrador.entity.Pessoa;

@Service
public class PessoaService {
	@Autowired
	private RestTemplate restTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;



    @Value("${br.com.microprojeto.orquestrador.url.consultar}")
    private String urlConsulta;

    @Value("${br.com.microprojeto.orquestrador.url.cadastrar}")
    private String urlCadastrar;

    public ResponseEntity<List> consultarPessoa(String cpfPessoa) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List> response = restTemplate.getForEntity(urlConsulta+cpfPessoa, List.class);
        return response;
    }

    public void cadastrarPessoa(Boolean fila, Pessoa pessoa) {
        if(fila.equals(false)) {
            restTemplate.postForObject(urlCadastrar, pessoa, Pessoa.class);
        }else {

            rabbitTemplate.convertAndSend(FilaConstante.FILA_CADASTRO, (new Gson()).toJson(pessoa));
        }
    }
}
