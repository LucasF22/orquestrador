package br.com.microprojeto.orquestrador.constante;

public class FilaConstante {
    public static final String FILA_ENTRADA = "ENTRADA";
    public static final String FILA_CADASTRO = "CADASTRO";
    public static final String FILA_RETORNO = "RETORNO";
}
