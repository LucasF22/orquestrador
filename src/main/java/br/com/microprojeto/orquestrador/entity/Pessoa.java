package br.com.microprojeto.orquestrador.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.amqp.core.Message;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Pessoa {
	private String id;
	private String nome;
	private String cpf;
	private String rg;
	private String logradouro;
	private String numLogradouro;
	private String cep;
	private String ddd;
	private String telefone;
}
