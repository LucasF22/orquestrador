package br.com.microprojeto.orquestrador.conection;

import br.com.microprojeto.orquestrador.constante.FilaConstante;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RabbitmqConection {
    private static final String NOME_EXCHANGE = "amq.direct";

    private AmqpAdmin amqpAdmin;

    public RabbitmqConection(AmqpAdmin amqpAdmin){
        this.amqpAdmin = amqpAdmin;
    }

    private Queue filaEntradaConsumer(String nomeFila){

        return new Queue(nomeFila, true, false, false);
    }
     private DirectExchange trocaDireta(){
        return new DirectExchange(NOME_EXCHANGE);
     }

     private Binding relacionamento(Queue fila, DirectExchange troca){
        return new Binding(fila.getName(), Binding.DestinationType.QUEUE, troca.getName(), fila.getName(), null);
     }

     @PostConstruct
    private void adicionaFila(){
        Queue filaEntrada = this.filaEntradaConsumer(FilaConstante.FILA_ENTRADA);
        Queue filaCadastro = this.filaEntradaConsumer(FilaConstante.FILA_CADASTRO);
        Queue filaRetorno = this.filaEntradaConsumer(FilaConstante.FILA_RETORNO);

        DirectExchange troca =this.trocaDireta();

        Binding ligacaoEntrada = this.relacionamento(filaEntrada, troca);
        Binding ligacaoCadastro = this.relacionamento(filaCadastro, troca);
        Binding ligacaoRetorno = this.relacionamento(filaCadastro, troca);

        this.amqpAdmin.declareQueue(filaEntrada);
        this.amqpAdmin.declareQueue(filaCadastro);
        this.amqpAdmin.declareQueue(filaRetorno);


        this.amqpAdmin.declareExchange(troca);

        this.amqpAdmin.declareBinding(ligacaoEntrada);
        this.amqpAdmin.declareBinding(ligacaoCadastro);
        this.amqpAdmin.declareBinding(ligacaoRetorno);
    }
}
