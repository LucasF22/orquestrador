package br.com.microprojeto.orquestrador.controller;

import br.com.microprojeto.orquestrador.business.RuleEngine;
import br.com.microprojeto.orquestrador.entity.Pessoa;
import br.com.microprojeto.orquestrador.service.PessoaService;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private RuleEngine parameterPessoa;

    @GetMapping("/consultar/{cpfPessoa}")
    public ResponseEntity<List> consultarPessoa(@PathVariable String cpfPessoa){
        Map<HttpStatus, String> listaErrosValidacao = parameterPessoa.parametrosCpf(cpfPessoa);
        ResponseEntity<List> respostaConsulta;
        ResponseEntity<List> resultado;
        Map<HttpStatus, String>  naoLocalizado;

        if(listaErrosValidacao.isEmpty()) {
            resultado = pessoaService.consultarPessoa(cpfPessoa);
            naoLocalizado = parameterPessoa.cpfNaoLocalizado(resultado);
            if (naoLocalizado.isEmpty()){
                respostaConsulta = new ResponseEntity(resultado, HttpStatus.OK);
            }else {
                respostaConsulta = new ResponseEntity(naoLocalizado, HttpStatus.NO_CONTENT);
            }

        }else{
            respostaConsulta = new ResponseEntity(listaErrosValidacao.values(), HttpStatus.NOT_FOUND);
        }
       
        return respostaConsulta;
    }


    @PostMapping("/cadastrar")
    public ResponseEntity<Void> cadastrarPessoa(@RequestBody Pessoa pessoa){
        Map<HttpStatus, String> listaErrosValidacao = parameterPessoa.parametroCadastro(pessoa);
        ResponseEntity cadastro;
        if (listaErrosValidacao.isEmpty()){
            pessoaService.cadastrarPessoa(false, pessoa);
            cadastro = new ResponseEntity("Cadastro realizado com sucesso!", HttpStatus.OK);
        }else {
            cadastro = new ResponseEntity(listaErrosValidacao.values(), HttpStatus.NOT_FOUND);
        }

        return cadastro;

    }
}

