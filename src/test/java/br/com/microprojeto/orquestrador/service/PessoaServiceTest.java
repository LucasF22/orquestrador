package br.com.microprojeto.orquestrador.service;

import br.com.microprojeto.orquestrador.entity.Pessoa;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PessoaServiceTest {

    @InjectMocks
    PessoaService pessoaService = new PessoaService();
    @Mock
    RestTemplate restTemplate;
    @Mock
    RabbitTemplate rabbitTemplate;

//    @Test
//    void consultarPessoa() {
//        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
//        List fakeListData = new ArrayList();
//        fakeListData.add(pessoa);
//        ResponseEntity<List> fakeResponse = new ResponseEntity<List>(fakeListData, HttpStatus.OK);
//        String s = "http://localhost:8081/pessoa/consultar/";
//        String cpf = "12345678910";
//        Mockito.when(restTemplate.getForEntity("http://localhost:8081/pessoa/consultar/12345678910", List.class))
//                .thenReturn(fakeResponse);
//        ResponseEntity<List> service = pessoaService.consultarPessoa(cpf);
//        assertEquals(fakeResponse, service);
//    }

    @Test
    void devePeloHttpCadastrarPessoa() {
        Pessoa pessoa = new Pessoa("", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        pessoaService.cadastrarPessoa(false, pessoa);
    }
    @Test
    void devePelaFilaCadastrarPessoa() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        pessoaService.cadastrarPessoa(true, pessoa);
    }
}