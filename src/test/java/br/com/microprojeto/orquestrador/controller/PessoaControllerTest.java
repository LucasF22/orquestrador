package br.com.microprojeto.orquestrador.controller;

import br.com.microprojeto.orquestrador.business.RuleEngine;
import br.com.microprojeto.orquestrador.entity.Pessoa;
import br.com.microprojeto.orquestrador.service.PessoaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PessoaControllerTest {
    @InjectMocks
    PessoaController pessoaControllerToTest;


    @Mock
    PessoaService pessoaServiceToMock;
    @Mock
    RuleEngine ruleEngineToMock;
    private Object List;


    @Test
    void deveConsultarPesssoa() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        List fakeListData = new ArrayList();
        fakeListData.add(pessoa);
        ResponseEntity<List> fakeResponse = new ResponseEntity<>(fakeListData,HttpStatus.OK);
        Map<HttpStatus, String> fakeError = new HashMap<>();

        Mockito.when(ruleEngineToMock.parametrosCpf("12345678910")).thenReturn(fakeError);
        Mockito.when(pessoaServiceToMock.consultarPessoa("12345678910")).thenReturn(fakeResponse);

        assertTrue((pessoaControllerToTest.consultarPessoa("12345678910"))
                .getStatusCode().equals(HttpStatus.OK));
    }
    @Test
    void naoDeveLocalizarConsultarPesssoa() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.NO_CONTENT, "Pessoa não localizada.");
        Mockito.when(ruleEngineToMock.cpfNaoLocalizado(null)).thenReturn(fakeError);
        assertTrue((pessoaControllerToTest.consultarPessoa("12345678910"))
                .getStatusCode().equals(HttpStatus.NO_CONTENT));
    }
    @Test
    void naoDeveConsultarPesssoa() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        Mockito.when(ruleEngineToMock.parametrosCpf("123456789")).thenReturn(fakeError);
        assertTrue((pessoaControllerToTest.consultarPessoa("123456789"))
                .getStatusCode().equals(HttpStatus.NOT_FOUND));
    }

    @Test
    void cadastrarPessoa() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        Mockito.when(ruleEngineToMock.parametroCadastro(pessoa)).thenReturn(fakeError);
        assertTrue((pessoaControllerToTest.cadastrarPessoa(pessoa))
                .getStatusCode().equals(HttpStatus.OK));
    }
    @Test
    void naoDeveCadastrarPessoa() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        Mockito.when(ruleEngineToMock.parametroCadastro(pessoa)).thenReturn(fakeError);
        assertTrue((pessoaControllerToTest.cadastrarPessoa(pessoa))
                .getStatusCode().equals(HttpStatus.NOT_FOUND));
    }


}