package br.com.microprojeto.orquestrador.business;


import br.com.microprojeto.orquestrador.entity.Pessoa;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RuleEngineTest {
    @InjectMocks
    RuleEngine ruleEngineToTest;


    @Test
    void semErrorParametrosCpf() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        assertEquals(fakeError, ruleEngineToTest.parametrosCpf("12345678910"));
    }
    @Test
    void nullParametrosCpf() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        fakeError.put(HttpStatus.BAD_REQUEST, "Parâmetro não informado.");
        assertEquals(fakeError, ruleEngineToTest.parametrosCpf(null));
    }
    @Test
    void vazioParametrosCpf() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        fakeError.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        assertEquals(fakeError, ruleEngineToTest.parametrosCpf(""));
    }
    @Test
    void tamanhoErradoParametrosCpf() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        assertEquals(fakeError, ruleEngineToTest.parametrosCpf("123"));
    }
    @Test
    void comLetrasParametrosCpf() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dado Incorreto.");
        assertEquals(fakeError, ruleEngineToTest.parametrosCpf("123asd45610"));
    }


    @Test
    void deveCpfLocalizado() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        List fakeListData = new ArrayList();
        fakeListData.add(pessoa);
        ResponseEntity<List> fakeResponse = new ResponseEntity<>(fakeListData,HttpStatus.OK);
        Map<HttpStatus, String> fakeError = new HashMap<>();
        assertEquals(fakeError, ruleEngineToTest.cpfNaoLocalizado(fakeResponse));
    }
    @Test
    void cpfNaoLocalizado() {
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.NO_CONTENT, "Pessoa não localizada.");
        assertEquals(fakeError, ruleEngineToTest.cpfNaoLocalizado(null));
    }

    @Test
    void semErrorParemetroCadastro() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12345678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "76801098", "69", "97254127");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        assertEquals(fakeError, ruleEngineToTest.parametroCadastro(pessoa));
    }
    @Test
    void nullParemetroCadastro() {
        Pessoa pessoa = new Pessoa(null, null, null, null, null, null, null, null, null);
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        fakeError.put(HttpStatus.BAD_REQUEST, "Parâmetros não informado.");
        assertEquals(fakeError, ruleEngineToTest.parametroCadastro(pessoa));
    }
    @Test
    void vazioParemetroCadastro() {
        Pessoa pessoa = new Pessoa("", "", "", "", "", "", "", "", "");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        fakeError.put(HttpStatus.NOT_ACCEPTABLE, "Parâmetros invalidos.");
        assertEquals(fakeError, ruleEngineToTest.parametroCadastro(pessoa));
    }
    @Test
    void tamanhoErradoParemetroCadastro() {
        Pessoa pessoa = new Pessoa("", "Elisa Allana Costa", "12378910", "329204518123456", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "786", "7680109813231", "6922", "972541272222");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        assertEquals(fakeError, ruleEngineToTest.parametroCadastro(pessoa));
    }
    @Test
    void comLetrasParemetroCadastro() {
        Pessoa pessoa = new Pessoa("1", "Elisa Allana Costa", "12asd678910", "329204518", "Rua Marechal Deodoro, Centro - Porto Velho - RO", "78a", "7680109as", "6a", "972541as");
        Map<HttpStatus, String> fakeError = new HashMap<>();
        fakeError.put(HttpStatus.I_AM_A_TEAPOT, "Dados Incorreto.");
        assertEquals(fakeError, ruleEngineToTest.parametroCadastro(pessoa));
    }
}